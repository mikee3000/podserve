from sys import argv, exit
from os import path, listdir
from htmlfy import htmlfy
from assemble import assemble
from math import floor
from time import strptime, strftime

usage = """
PodServe - adds new podcast episodes to a website and rss feed
usage:
    podserve input_dir webroot_dir season episode pubdate duration ogg_length mp3_length
"""


# make sure all arguments are provided
try:
    show_notes = ''
    input_dir = path.join(argv[1])
    for file in listdir(input_dir):
        if file.endswith('.md'):
            show_notes = file
    html_file = show_notes.replace("md", "-shownotes.html")
    show_notes_path = path.join(input_dir, show_notes)
    html_path = path.join(input_dir, html_file)
    webroot_dir = path.join(argv[2])
    season = argv[3]
    episode = argv[4]
    pubdate = argv[5]
    duration = argv[6]
    ogg_length = argv[7]
    mp3_length = argv[8]

except (IndexError, NameError):
    print(usage)
    exit()

# create a nice looking date for the website
t = strptime(pubdate, "%a, %d %b %Y %H:%M:%S +0000")
ordinal = lambda n: "%d%s" % (n,"tsnrhtdd"[(floor(n/10)%10!=1)*(n%10<4)*n%10::4])
nicedate = str(strftime("%B", t) + " " + str(ordinal(t.tm_mday)) + " " + str(t.tm_year))

# parse the shownotes into a html file in the input dir
htmlfy(show_notes_path, html_path)
# update the website and the feeds and put audio files into the right places
assemble(input_dir, webroot_dir, season, episode, pubdate, nicedate, duration, ogg_length, mp3_length)
