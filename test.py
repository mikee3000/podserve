from html.parser import HTMLParser
class MyHTMLParser(HTMLParser):
    def handle_data(self, data):
        self.parsed = data

parser = MyHTMLParser( convert_charrefs=True)
z = parser.feed("""<h3>Linux Lads - Season 1 - Episode 3 - Let&rsquo;s stand up for the national anthem</h3>""")
print(parser.parsed)
