<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Linux Lads Podcast</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="include.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style.css">
        <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    </head>
    <body>
                 
        <!-- this css changes the color of the links so that inactive links are visible -->
        <style>
            .nav-tabs > li > a {
                color: white;
                border: medium none;
                border-radius: 0;
            }
            .nav-tabs > li > a:hover {
                background-color: white;
                color:#1793D1;
            }
        </style>
                
        <div class="container-fluid">
            <div class="row content">
            <div class="col-md-2 sidebar">
            <img class="img-responsive" src="images/linux_lads_logo_web_sidebar.png"></img>
                    
            <!-- Fancy nav menu for when we have more content-->
                    
            <ul class="nav nav-tabs nav-stacked">
                <li><a href="https://linuxlads.com" target="_blank">Home</a></li>
                <li><a href="https://linuxlads.com/feed_ogg.rss" target="_blank">Ogg Feed</a></li>
                <li><a href="https://linuxlads.com/feed_mp3.rss" target="_blank">MP3 Feed</a></li>
                <!--<li><a data-toggle="tab" href="#AboutUs">About Us</a></li>-->
            </ul>
        </div>
                
        <div class="col-md-10">
            <div class="col-md-8">
                <div class="tab-content main-content-div">
                    <div id="AboutUs" class="tab-pane fade">
                        We are we are... the youth of the nation
                    </div>
                    <div id="publish_form" class="tab-pane fade in active">
						<h2>The Linux Lads Podcast Publishing Interface</h2>
						<form method="post" action="publish.php" enctype="multipart/form-data">
							Date: <input type="date" name="date"><br>
							Shownotes: <input type="file" name="upload[]" id="shownotes"><br>
							Season: <input type="number" name="season"><br>
							Episode: <input type="number" name="episode"><br>
							ogg File: <input type="file" name="upload[]" id="ogg"><br>
							mp3 File: <input type="file" name="upload[]" id="mp3"><br>
							<input type="submit" name="generate" value="Generate" />
						</form>
					</div>
                </div>
            <div class="col-md-4">
            </div>
        </div>
        <footer class="container-fluid"> 
        </footer>
        <script>
            includeHTML();
        </script> 
    </body>
</html>

