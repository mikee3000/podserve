#!/usr/bin/env python
from os import path
import codecs

import sys
import markdown

# turn the markdown show notes into html
def htmlfy(show_notes_path, html_path):
    with codecs.open(show_notes_path, 'r', encoding="utf-8") as md:
        text = md.read()
        extensions = ['extra', 'smarty']
        html = markdown.markdown(text, extensions=extensions, output_format='html5')
    with open(html_path, 'w') as hp:
        hp.write(html)
