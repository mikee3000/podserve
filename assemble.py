from os import listdir, makedirs, path, getcwd
from shutil import copyfile
from jinja2 import Environment, PackageLoader, select_autoescape
from html.parser import HTMLParser


# create a html-free title for the feed
class MyHTMLParser(HTMLParser):
    def handle_data(self, data):
        self.parsed = data

def get_title_rss(title):
    parser = MyHTMLParser()
    parser.feed(title)
    title_rss = parser.parsed
    return title_rss



def assemble(input_dir, webroot_dir, season, episode, pubdate, nicedate, duration, ogg_length, mp3_length):
    # this sets the template
    env = Environment(
        loader=PackageLoader('podserve', 'templates'),
        autoescape=select_autoescape(['html', 'xml'])
    )
    # the absolute dir path to the local episode dir
    episode_path = path.join(webroot_dir, 'podcast', season, episode)
    # the episode path relative to the neocities dir where index.html is
    rel_episode_path = path.join('podcast', season, episode)
    # create the podcast/season/episode directories if they don't exist
    if not path.exists(episode_path):
        makedirs(episode_path)
    # podcast dir path
    pod_dir = path.join(webroot_dir, 'podcast')
    # if the command line arg for input_dir is '.' change it to the script dir
    if input_dir == '.':
        input_dir = getcwd()
    # set up paths for the feeds
    rss_file_o = path.join(webroot_dir, 'feed_ogg.rss')
    new_rss_file_o = path.join(episode_path, 'new_feed_ogg.rss')
    rss_file_m = path.join(webroot_dir, 'feed_mp3.rss')
    new_rss_file_m = path.join(episode_path, 'new_feed_mp3.rss')
    # current episode ogg and mp3 files
    ogg_file = ''
    mp_file = ''
    # the episode title
    title = ''
    # the body of the show notes
    content = ''
    # the short summary of the episoded
    summary = ''


    # check all the files in the input_dir
    for file in listdir(input_dir):
        # move the ogg file into the episode folder
        if file.endswith('.ogg'):
            ogg_file = path.join(input_dir, file)
            ogg_file_placed = path.join(episode_path, file)
            pt_path_ogg = path.join('https://dts.podtrac.com/redirect.ogg/linuxlads.com/podcast/', season, episode, file) 
            copyfile(ogg_file, ogg_file_placed)
            ogg_file = path.join(rel_episode_path, file)
        # then the mp3 file
        elif file.endswith('.mp3'):
            mp_file = path.join(input_dir, file)
            mp_file_placed = path.join(episode_path, file)
            copyfile(mp_file, mp_file_placed)
            pt_path_mp3 = path.join('https://dts.podtrac.com/redirect.mp3/linuxlads.com/podcast/', season, episode, file) 
            mp_file = path.join(rel_episode_path, file)
        # read the shownotes file created from md by htmlfy.py
        elif file.endswith('-shownotes.html'):
            html_file = path.join(input_dir, file)
            with open(html_file, 'r') as hfile:
                counter = 0
                for line in hfile:
                    counter += 1
                    # select the first line as the title
                    if counter == 1:
                        title = line
                        title = title.rstrip()
                    # select the second line as the summary
                    elif counter == 2:
                        summary = line
                        summary = summary.rstrip()
                    # the other lines form the body of the show notes
                    else:
                        content += line
        else:
            continue

    title_rss = get_title_rss(title)

    # create the rss feeds
    # ogg feed
    xo_template = env.get_template('xml_template_ogg')
    # rss_file_o
    with open(new_rss_file_o, 'w') as xo_file:
        xo_file.write(xo_template.render(title=title_rss, summary=summary, ogg_file_placed=pt_path_ogg, ogg_length=ogg_length, duration=duration, pubdate=pubdate, content=content, mppath=mp_file))
    # mp3 feed
    xm_template = env.get_template('xml_template_mp3')
    # rss_file_m
    with open(new_rss_file_m, 'w') as xm_file:
        xm_file.write(xm_template.render(title=title_rss, summary=summary, mp_file_placed=pt_path_mp3, mp3_length=mp3_length, duration=duration, pubdate=pubdate, content=content, mppath=mp_file))

    # glue all the feeds together
    whole_ogg_rss = ''
    whole_ogg_rss_arr = [] # need to reverse the order, listdir goes asc, but newest episodes need to be on top
    # later found out that the order is probably random, added sorted(), this needs tidying up big time
    with open('podserve/templates/rss_top_ogg', 'r') as rto:
        whole_ogg_rss = rto.read()
    for sfold in sorted(listdir(pod_dir)):
        for efold in sorted(listdir(path.join(pod_dir, sfold))):
            for e_file in listdir(path.join(pod_dir, sfold, efold)):
                if e_file == 'new_feed_ogg.rss':
                    with open(path.join(pod_dir, sfold, efold, e_file), 'r') as rfile:
                        whole_ogg_rss_arr.append(rfile.read())

    whole_mp3_rss = ''
    whole_mp3_rss_arr = []
    with open('podserve/templates/rss_top_mp3', 'r') as rto:
        whole_mp3_rss = rto.read()
    for sfold in sorted(listdir(pod_dir)):
        for efold in sorted(listdir(path.join(pod_dir, sfold))):
            for e_file in listdir(path.join(pod_dir, sfold, efold)):
                if e_file == 'new_feed_mp3.rss':
                    with open(path.join(pod_dir, sfold, efold, e_file), 'r') as rfile:
                        whole_mp3_rss_arr.append(rfile.read())

    for itm in reversed(whole_ogg_rss_arr):
        whole_ogg_rss += '\n' + itm
    for itm in reversed(whole_mp3_rss_arr):
        whole_mp3_rss += '\n' + itm

    whole_ogg_rss += "</channel>\n</rss>"
    whole_mp3_rss += "</channel>\n</rss>"
    with open(path.join(webroot_dir, 'new_feed_ogg.rss'), 'w') as nofr:
        nofr.write(whole_ogg_rss)
    with open(path.join(webroot_dir, 'new_feed_mp3.rss'), 'w') as nofr:
        nofr.write(whole_mp3_rss)

    # load the template from the template folder
    template = env.get_template('html_template')
    # create the paths for the episode html file
    ep_html_file = 's' + season + '_e' + episode + '.html'
    ep_html_path = path.join(episode_path, ep_html_file)
    # ... and the relative file
    rel_ep_html_path = path.join(rel_episode_path, ep_html_file)
    # create the episode html file
    with open(ep_html_path, 'w') as e_h_file:
        e_h_file.write(template.render(title=title, summary=summary, pubdate=pubdate, nicedate=nicedate, content=content, oggpath=pt_path_ogg, mppath=pt_path_mp3))
    # add the and 'include' div for the episode to the main site's html
    with open(path.join(webroot_dir, 'index.html'), 'r') as index_file:
        new_index = ''
        # a flag to remove the 'active' attribute from the menu item on the next line
        remove_active = False
        remove_active_ediv = False
        # pass the current index.html and add new entries
        for num, line in enumerate(index_file):
            # deactivate the last entry in the menu
            if remove_active == True:
                line = line.replace(' class="active"', '')
                remove_active = False
            # change the current active div to non-active
            if remove_active_ediv == True:
                line = line.replace('tab-pane fade in active', 'tab-pane fade in')
                remove_active_ediv = False
            new_index += line
            # add new episode to the commented list
            if line.find("<!-- PODSERVE LIST OF EPISODES -->") != -1:
                new_index += '\t\t\t\t\t\t<!-- ' + season + ' ' + episode +  ' -->\n'		
            # add new episode to the menu
            elif line.find("<!-- INCLUDE MENU ENTRIES USED BY PODSERVE -->") != -1:
                new_index += '\t\t\t\t<li class="active"><a data-toggle="tab" href="#s' + season + 'e' + episode + '">Season ' + season.lstrip("0") + ' - Episode ' + episode.lstrip("0") + '</a></li>\n'
                remove_active = True
            # add the new episode's div
            elif line.find("<!-- PODSERVE ADD NEW EPISODE DIV HERE -->") != -1:
                remove_active_ediv = True
                new_index += '\t\t\t\t\t<div id="s' + season + 'e' + episode + '" class="tab-pane fade in active">\n'	
                new_index += '\t\t\t\t\t\t<div w3-include-html="' + rel_ep_html_path + '"></div>\n'		
                new_index += '\t\t\t\t\t</div>\n'
    # create new index.html
    new_index_file = path.join(webroot_dir, 'new_index.html')
    with open(new_index_file, 'w') as n_index_file:
        n_index_file.write(new_index)
                 
        
       
